<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-5 shadow rounded-4 p-4">
            <h1 class="fw-bold">Login</h1>
            <?= Flasher::generalFlash() ?>
            <form class="mt-4" method="POST" action="<?= BASEURL ?>/login/loginprocess">
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
                </div>
                <div class="mb-2">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" id="password" class="form-control" name="password" placeholder="Password">
                </div>
                <div class="row">
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>