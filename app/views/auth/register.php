<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-5 shadow rounded-4 p-4">
            <h1 class="fw-bold">Register</h1>
            <?= Flasher::generalFlash() ?>
            <form class="mt-4" method="POST" action="<?= BASEURL ?>/register/regisprocess">
                <div class="mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="nama_penulis" placeholder="Nama Lengkap">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
                </div>
                <div class="row">
                    <div class="col">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" id="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <div class="col">
                        <label for="confirmpass" class="form-label">Confirm Password</label>
                        <input type="password" id="confirmpass" class="form-control" name="confirmpass" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="row">
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>