<div class="container mt-4">
    <h1>Ubah Data User</h1>
    <div class="row">
        <div class="col-6">
            <form action="<?= BASEURL ?>/blog/editproses" method="POST">
                <input type="hidden" name="id" value="<?= $data['blog']['id'] ?>">
                <input type="hidden" name="penulis" value="<?= $data['blog']['penulis'] ?>">
                <div class="mb-3">
                    <label for="judul" class="form-label">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="<?= $data['blog']['judul'] ?>">
                </div>
                <div class="mb-3">
                    <label for="tulisan" class="form-label">Tulisan</label>
                    <input type="text" class="form-control" id="tulisan" name="tulisan" value="<?= $data['blog']['tulisan'] ?>">
                </div>
                <button type=" submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>