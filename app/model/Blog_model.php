<?php
class Blog_model
{
    private $db;
    private $table = 'blog';
    public function __construct()
    {
        $this->db = new Database();
    }
    public function getAllBlog()
    {
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultSet();
    }

    public function getBlogById($id)
    {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        return $this->db->single();
    }
    public function getBlogByUser()
    {
        $this->db->query("SELECT * FROM {$this->table} WHERE penulis = :penulis");
        $this->db->bind('penulis', $_SESSION['user']['id']);
        return $this->db->resultSet();
    }

    public function hapusBlog($id)
    {
        $this->db->query("DELETE FROM $this->table WHERE id = :id");
        $this->db->bind('id', $id);
        return $this->db->execute();
    }

    public function tambahBlog($data)
    {
        $this->db->query("INSERT INTO $this->table VALUES (null, :penulis, :judul, :tulisan)");
        $this->db->bind('penulis', $data['penulis']);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('tulisan', $data['tulisan']);
        $this->db->execute();
    }

    public function namaPenulis($id)
    {
        $this->db->query("SELECT nama_penulis FROM user WHERE user.id = $id");
        $this->db->execute();
        return $this->db->single();
    }

    public function editBlog($data)
    {
        $this->db->query("UPDATE $this->table SET judul = :judul, tulisan = :tulisan, penulis = :penulis WHERE id = :id");
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('penulis', $data['penulis']);
        $this->db->bind('id', $data['id']);
        $this->db->bind('tulisan', $data['tulisan']);
        $this->db->execute();
    }
}
