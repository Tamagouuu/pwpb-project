<?php
class Blog extends Controller
{



    public function index()
    {
        $data["judul"] = "Blog";
        $data["blog"] = $this->model("Blog_model")->getBlogByUser();
        $data["user"] = $this->model('User_model')->getAllUser();
        $this->view("templates/header", $data);
        $this->view("blog/index", $data);
        $this->view("templates/footer");
    }

    public function tambah()
    {
        $this->model('Blog_model')->tambahBlog($_POST);
        header('Location:' . BASEURL . '/blog');
    }

    public function hapus($id)
    {
        $this->model('Blog_model')->hapusBlog($id);
        header('Location:' . BASEURL . '/blog');
    }

    public function detail($id)
    {
        $data['judul'] = "Detail Blog";
        $data['blog'] = $this->model("Blog_model")->getBlogById($id);
        $data['penulis'] = $this->model("User_model")->getUserById($data['blog']['penulis']);
        $this->view('templates/header', $data);
        $this->view('blog/detail', $data);
        $this->view('templates/footer');
    }

    public function edit($id)
    {
        $data['judul'] = 'Ubah Blog';
        $data['blog'] = $this->model('Blog_model')->getBlogById($id);
        $this->view("templates/header", $data);
        $this->view("blog/edit", $data);
        $this->view("templates/footer", $data);
    }

    public function editProses()
    {
        $this->model('Blog_model')->editBlog($_POST);
        header('Location:' . BASEURL . '/blog');
    }
}
