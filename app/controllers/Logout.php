<?php

class Logout extends Controller
{
    public function __construct()
    {
        if ($_SESSION['user']) {
            header('Location:' . BASEURL . '/');
        }
    }


    public function index()
    {
        session_destroy();
        header('Location:' . BASEURL . '/login');
    }
}
