<?php

class Register extends Controller
{
    public function __construct()
    {
        if (isset($_SESSION['user'])) {
            header('Location:' . BASEURL . '/');
        }
    }


    public function index()
    {
        $data['judul'] = 'Register';
        $this->view('templates/auth/header', $data);
        $this->view('auth/register', $data);
        $this->view('templates/auth/footer');
    }

    public function regisProcess()
    {
        if ($_POST['password'] == $_POST['confirmpass']) {
            if ($this->model('User_model')->getUserByEmail($_POST['email'])) {
                Flasher::setFlash('Akun sudah', 'terdaftar', 'danger');
                header('Location: ' . BASEURL . '/register');
                exit;
            } else {
                if ($this->model('User_model')->tambahUser($_POST) > 0) {
                    Flasher::setFlash('Akun berhasil', 'didaftarkan', 'success');
                    header('Location: ' . BASEURL . '/login');
                    exit;
                } else {
                    Flasher::setFlash('Akun gagal', 'didaftarkan', 'danger');
                    header('Location: ' . BASEURL . '/register');
                    exit;
                }
            }
        } else {
            Flasher::setFlash('Confirm password', 'tidak sesuai', 'danger');
            header('Location:' . BASEURL . '/register');
        }
    }
}
