<?php
class User extends Controller
{
  public function index()
  {
    $data['judul'] = 'User';
    $data['user'] = $this->model('User_model')->getAllUser();
    $this->view("templates/header", $data);
    $this->view("user/index", $data);
    $this->view("templates/footer", $data);
  }

  public function profile($nama = 'Linux', $pekerjaan = 'Devs')
  {
    $data['judul'] = 'Profile';
    $data["nama"] = $nama;
    $data["pekerjaan"] = $pekerjaan;
    $this->view("templates/header", $data);
    $this->view("user/profile", $data);
    $this->view("templates/footer", $data);
  }

  public function tambah()
  {
    $this->model('User_model')->tambahUser($_POST);
    header('Location:' . BASEURL . '/user');
  }

  public function hapus($id)
  {
    $this->model('User_model')->hapusUser($id);
    header('Location:' . BASEURL . '/user');
  }

  public function edit($id)
  {
    $data['judul'] = 'Ubah User';
    $data['user'] = $this->model('User_model')->getUserById($id);
    $this->view("templates/header", $data);
    $this->view("user/edit", $data);
    $this->view("templates/footer", $data);
  }

  public function editProses()
  {
    $this->model('User_model')->editUser($_POST);
    header('Location:' . BASEURL . '/user');
  }
}
