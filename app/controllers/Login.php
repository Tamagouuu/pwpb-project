<?php

class Login extends Controller
{
    public function __construct()
    {
        if (isset($_SESSION['user'])) {
            header('Location:' . BASEURL);
        }
    }

    public function index()
    {
        $data['judul'] = 'Login';
        $this->view('templates/auth/header', $data);
        $this->view('auth/login', $data);
        $this->view('templates/auth/footer');
    }

    public function loginprocess()
    {
        if ($this->model('User_model')->getUserByEmail($_POST['email'])) {
            $result = $this->model('User_model')->getUserByEmail($_POST['email']);
            if (md5($_POST['password'] . SALT) == $result['password']) {
                $_SESSION['user'] = [
                    'id' => $result['id'],
                    'nama' => $result['nama_penulis'],
                    'email' => $result['email'],
                ];
                Flasher::setFlash('Selamat datang', "{$result['nama_user']}", 'success');
                header('Location: ' . BASEURL . '/blog');
                exit;
            } else {
                Flasher::setFlash('Password', 'salah', 'danger');
                header('Location: ' . BASEURL . '/login');
                exit;
            }
        } else {
            Flasher::setFlash('Akun tidak', 'terdaftar', 'danger');
            header('Location: ' . BASEURL . '/login');
            exit;
        }
    }
}
