<?php
class Home extends Controller
{
    public function __construct()
    {
        if (!$_SESSION['user']) {
            header('Location:' . BASEURL . '/login');
        }
    }

    public function index($nama = 'Gautama', $pekerjaan = 'koruptor')
    {
        $data["judul"] = "Home";
        $data["nama"] = "Gautama";
        $data["pekerjaan"] = $pekerjaan;
        $this->view("templates/header", $data);
        $this->view("home/index", $data);
        $this->view("templates/footer", $data);
    }
}
